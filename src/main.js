import { createApp } from 'vue';
import App from './App.vue';
import router from './router';
import PrimeVue from 'primevue/config';
import Button from 'primevue/button';
import InputText from 'primevue/inputtext';
import TabView from 'primevue/tabview';
import TabPanel from 'primevue/tabpanel';
import InputMask from 'primevue/inputmask';
import Dropdown from 'primevue/dropdown';
import {Swiper, SwiperSlide} from 'swiper/vue';
import StarRating from 'vue-star-rating'
import './assets/scss/main.scss';
import 'primevue/resources/themes/saga-blue/theme.css'; //theme
import 'primevue/resources/primevue.min.css'; //core css
import 'primeicons/primeicons.css'; //icons
import 'swiper/swiper-bundle.css'

const app = createApp(App);
app.use(PrimeVue);
app.use(router);

app.component('Button',Button);
app.component('InputText',InputText);
app.component('TabView',TabView);
app.component('TabPanel',TabPanel);
app.component('Swiper',Swiper);
app.component('SwiperSlide',SwiperSlide);
app.component('StarRating',StarRating);
app.component('InputMask',InputMask);
app.component('Dropdown',Dropdown);

app.mount('#app')
